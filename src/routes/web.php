<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// routing for books
Route::get('/', [\App\Http\Controllers\BookController::class, 'index']);
Route::post('/add', [\App\Http\Controllers\BookController::class, 'store']);
Route::get('/book/{book}', [\App\Http\Controllers\BookController::class, 'edit']);
Route::put('/book/{book}', [\App\Http\Controllers\BookController::class, 'update']);
Route::delete('/book/{book}', [\App\Http\Controllers\BookController::class, 'destroy']);
Route::get('/export', [\App\Http\Controllers\BookController::class, 'export']);