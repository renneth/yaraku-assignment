<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;

$factory->faker->seed('demo');
$factory->define(Book::class, function (Faker $faker) {
    return [
        'title' => str_replace(".","",$faker->sentence(3)),
        'author' => $faker->name,
    ];
});
