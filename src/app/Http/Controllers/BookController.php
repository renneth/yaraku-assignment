<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use ACFBentveld\XML\XML;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $request->input('title');
        $author = $request->input('author');
        
        $books = Book::sortable()
            ->where('title','LIKE','%'.$title.'%')
            ->where('author','LIKE','%'.$author.'%')
            ->orderBy('id', 'DESC')
            ->paginate(5);

	    return view('book.index', [
            'books' => $books,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new = Book::create([
            'title' => $request->title,
            'author' => $request->author
        ]);

        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view('book.edit', [
            'book' => $book,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $book->update([
            'title' => $request->title,
            'author' => $request->author
        ]);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect('/');
    }

    /**
     * Export listing based on filter and current sorting vars
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {
        $columnSet = $request->input('column');
        $format = $request->input('format');
        $title = $request->input('title');
        $author = $request->input('author');

        $fileName = ($format == "xml") ? 'books.xml' : 'books.csv';
        $books = Book::sortable()
            ->orderBy('id', 'DESC')
            ->when($title, function ($query, $data) {
                return $query->where('title','LIKE','%'.$data.'%');
            })->when($author, function ($query, $data) {
                return $query->where('author','LIKE','%'.$data.'%');
            })->get();

        $headers = array(
            "Content-type"        => $format == "xml" ? "text/xml" : "text/csv" ,
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $callbackXML  = function() use($books, $columnSet) {
            $booksXML = new \SimpleXMLElement("<books></books>");
            foreach ($books as $book) {
                $bookXML = $booksXML->addChild('book');
                switch($columnSet) {
                    case 'titles': 
                        $bookXML->addChild('id',$book->id);
                        $bookXML->addChild('title',$book->title);
                        break;
                    case 'authors': 
                        $bookXML->addChild('id',$book->id);
                        $bookXML->addChild('author',$book->author);
                        break;
                    default:
                        $bookXML->addChild('id',$book->id);
                        $bookXML->addChild('title',$book->title);
                        $bookXML->addChild('author',$book->author);
                }
            }
            echo $booksXML->asXML();
        };

        $callbackCSV = function() use($books, $columnSet) {
            $file = fopen('php://output', 'w');
            switch($columnSet) {
                case 'titles': $columns = ['ID','Title']; break;
                case 'authors': $columns = ['ID','Author']; break;
                default: $columns = ['ID','Title','Author'];
            }
            fputcsv($file, $columns);
            foreach ($books as $book) {
                switch($columnSet) {
                    case 'titles': $row = [$book->id,$book->title]; break;
                    case 'authors': $row = [$book->id,$book->author]; break;
                    default: $row = [$book->id,$book->title,$book->author]; 
                }
                fputcsv($file,$row);
            }
            fclose($file);
        };

        return response()->streamDownload($format == "xml" ? $callbackXML : $callbackCSV, $fileName, $headers);

    }

}
