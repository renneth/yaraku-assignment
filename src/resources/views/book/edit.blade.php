@extends('layouts.app')
@section('content')

<div class="container pt-5">

	<h1>Edit {{ config('app.name') }}</h1>

	<div class="row">
		<div class="col-md-6 ">
			
			<form action="" method="POST" class="p-3 bg-light border">
				@csrf
				@method('PUT')
				<div class="row">
					<div class="control-group col-12 mb-2">
						<label class="control-label" for="title">Title</label>
						<input type="text" id="title" class="form-control" name="title" value="{{ $book->title }}" required>
					</div>
					<div class="control-group col-12">
						<label class="control-label" for="author">Author</label>
						<input type="text" id="author" class="form-control" name="author" value="{{ $book->author }}" required>
					</div>
				</div>
				<div class="row mt-3">
					<div class="control-group col-12">
						<a type="button" href="/" class="btn btn-secondary btn-sm"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
						<button id="btn-submit" class="btn btn-primary btn-sm">
							Save
						</button>
					</div>
				</div>
			</form>
				
		</div>
	</div>
</div>

@endsection