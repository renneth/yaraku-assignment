@extends('layouts.app')
@section('content')
<div class="container pt-5">

	<h1><a href="/">{{ config('app.name') }}</a></h1>

	@if (count($books))
	<div class="navigation mb-3">
		<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus" aria-hidden="true"></i> Add</button>
		<button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
		@if (count(request()->except(['_token'])))
		<a class="btn btn-secondary btn-sm" href="/"><i class="fa fa-times" aria-hidden="true"></i> Exit Search</a>
		@endif
		<button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#exportModal"><i class="fa fa-download" aria-hidden="true"></i> Export</button>
	</div>
	@endif

	<div class="row">
		<div class="col-12">

		@if (count($books))
			<table class="table">
				<thead class="thead-light">
					<tr>
					<th scope="col">@sortablelink('id', 'ID')</th>
					<th scope="col">@sortablelink('title', 'Title')</th>
					<th scope="col">@sortablelink('author', 'Author')</th>
					<th scope="col"></th>
					</tr>
				</thead>
			<tbody>
			@foreach($books as $book)
				<tr>
				<th scope="row">{{ $book->id }}</th>
				<td>{{ ucfirst($book->title) }}</td>
				<td>{{ ucfirst($book->author) }}</td>
				<td class="d-flex justify-content-end">
					<a href="./book/{{ $book->id }}" class="btn btn-secondary btn-sm mr-1" title="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
					<form id="delete-frm" onsubmit="return confirm('Delete this book?')" class="" action="./book/{{ $book->id }}" method="POST">
						@method('DELETE')
						@csrf
						<button class="btn btn-secondary btn-sm" title="delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
					</form>
				</td>
				</tr>
			@endforeach
			</tbody>
			</table>

			{!! $books->appends(request()->query())->render() !!}

		@else
		Empty list. <a href="#" data-toggle="modal" data-target="#addModal">Add one?</a>
		@endif
		</div>
	</div>


</div>

<div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="exportModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="/export?{{ http_build_query(request()->except(['_token'])) }}" method="GET">
			@csrf
			@foreach(request()->except(['_token']) as $index=>$value)
			<input type="hidden" name="{{$index}}" value="{{$value}}">
			@endforeach
			<div class="modal-header">
				<h5 class="modal-title" id="exportModalLabel">Export Books</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<label class="mr-sm-2 sr-only" for="columnsSelect">Columns</label>
				<select class="custom-select mr-sm-2 mb-2" id="columnsSelect" name="column">
					<option value="all" selected>All columns</option>
					<option value="titles">Titles only</option>
					<option value="authors">Authors only</option>
				</select>

				<label class="mr-sm-2 sr-only" for="formatSelect">Format</label>
				<select class="custom-select mr-sm-2 mb-2" id="formatSelect" name="format">
					<option value="csv" selected>CSV</option>
					<option value="xml">XML</option>
				</select>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button id="btn-submit" type="submit" class="btn btn-primary">Export</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="/" method="GET">
			@csrf
			<div class="modal-header">
				<h5 class="modal-title" id="searchModalLabel">Search</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="control-group my-2">
					<label class="control-label" for="title">Title</label>
					<input type="text" id="title" class="form-control" name="title"
						value="{{request()->query('title')}}">
				</div>
				<div class="control-group  my-2">
					<label class="control-label" for="author">Author</label>
					<input type="text" id="author" class="form-control" name="author"
						value="{{request()->query('author')}}">
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button id="btn-submit" type="submit" class="btn btn-primary">Search</button>
			</div>
			</form>
		</div>
	</div>
</div>



<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="/add" method="POST">
			@csrf
			<div class="modal-header">
				<h5 class="modal-title" id="addModalLabel">Add a Book</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="control-group my-2">
					<label class="control-label" for="title">Title</label>
					<input type="text" id="title" class="form-control" name="title"
							value="" required>
				</div>
				<div class="control-group  my-2">
					<label class="control-label" for="author">Author</label>
					<input type="text" id="author" class="form-control" name="author"
							value="" required>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button id="btn-submit" type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>


@endsection